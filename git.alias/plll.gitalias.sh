!f() {  # aliases start in $(git rev-parse --show-toplevel)
  set -e; cd "$(git rev-parse --git-common-dir)"/..; unset GIT_DIR; git fetch origin; set -x
  git merge --ff-only origin/master
  for b in bin setup links files; do (cd "local/$b" && git merge --ff-only origin/"$b"); done
}; f
