!f() {
for f; do case "$f" in /*) ;; *) unset Need; ${Need?absolute path: $f} ;; esac; done
set -eu; f=$1 t=$2
gcd=$(git rev-parse --git-common-dir)/..; cd -- "$gcd"; unset GIT_DIR; set -x
gcd=$(pwd -P)
eval "$(git for-each-ref --shell --format 'cd %(worktreepath)' refs/heads/links)"
test "$(git branch --show-current)" = links || return 1
git mv ./"$f" ./"$t"
git commit ./"$f" ./"$t" -m "Rename: $f -> $t"
ln -sfT "$t" ./"$t"
git commit ./"$t" -m "Update link: $t"
cd "$gcd"
eval "$(git for-each-ref --shell --format 'cd %(worktreepath)' refs/heads/files)"
test "$(git branch --show-current)" = files || return 1
git reset --hard
git mv ./data/"$f" ./data/"$t"
git mv ./meta/"$f" ./meta/"$t"
git commit ./data/"$f" ./data/"$t" ./meta/"$f" ./meta/"$t" -m "Rename: $f -> $t"
}; f
