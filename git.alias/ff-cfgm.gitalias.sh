!f() {
  set -e; cd "$(git rev-parse --git-common-dir)"/..; unset GIT_DIR; git fetch cfgm; set -x
  for b in bin setup; do (cd "local/$b" && git merge --ff-only cfgm/"$b"); done
}; f
